package com.example.verceldemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VercelDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(VercelDemoApplication.class, args);
	}

}
